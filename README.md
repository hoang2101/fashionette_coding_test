# Fashionette Coding Challenge
Create a JSON API, which will allow the consumer to search for TV
shows by their name, using a simple query string as a GET parameter like so:
https://json-api.local/?q=deadwood  
Any other request to the API is invalid and should return the appropriate response.
### Environment
PHP 7.4.5  
Composer 2.2.7  
Lumen 8.3.1  
PHPUnit 9.5  
Redis Server 5.0.7
### Usage

* Init install: `composer i`
* Copy `.env.example` to `.env` and config the Redis connection:
```
REDIS_HOST = //ex. 127.0.0.1
REDIS_PORT = //ex. 6379
REDIS_PASSWORD = //ex. null
```
* Start the API: `php -S localhost:8000 -t public`
* Make request: `GET http://localhost:8000/?q=MOVIE_TITLE`
* Run the test: `./vendor/bin/phpunit`  

The expected response status should be 200, and the content is always a JSON Array.
If the method is incorrect, the response status is 405.  
If the uri is invalid, the response status is 404.
----
The search is exact search since the title is also a Redis key.  
The API can be evolved as a 3rd party service to fetch data from any API with a better performance thanks to Redis.  
Things need to be improved:  

* Put the caching logic in the service only. The strategies only fetch the APIs and return an array.  
* Since I am not sure how the fuzzy algorithm works (ex. `q` as `Teacher` may also not contain ALL result of `Bad Teacher`), I didn't cache all result. If the return is the same, caching all should be considered.  
* Write the document in Swagger. 
* TV Show can be an Entity and map to the DB using ORM if needed. 
