<?php

namespace App\Http\Controllers;

use App\Http\Services\TVShowService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TVShowsController extends Controller
{
    private TVShowService $service;

    public function __construct(TVShowService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    public function getShows(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string|max:100',
        ]);
        return $this->service->showHandler($request->query('q'));
    }
}
