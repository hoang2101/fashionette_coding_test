<?php

namespace App\Http\Services;

use App\APIFetchers\TVShowFetcherInterface;
use Illuminate\Support\Facades\Redis;

class TVShowService
{
    private TVShowFetcherInterface $connector;

    /**
     * TVShowService constructor.
     * @param TVShowFetcherInterface $connector
     */
    public function __construct(TVShowFetcherInterface $connector)
    {
        $this->connector = $connector;
    }

    /**
     * Determine if the keyword has been request before (case-sensitive or not).
     * If keyword is already cached, then we will get the results from Redis. If not, make a new request and cache new results.
     * If the request is fail, we don't cache in Redis (in case their APIs have some problems, we still can try again).
     * Each time the query is success, we count how many time that keyword is used. This is very useful for statistics
     *
     * @param string $searchTerm
     * @return array
     */
    public function showHandler(string $searchTerm): array
    {
        //use md5 to make sure the keys have no white spaces.
        $redisSearchKey = env('SEARCHED_TERM_PREFIX') . md5(strtolower(trim($searchTerm)));
        $searchedTimes = Redis::get($redisSearchKey);
        if ($searchedTimes) {
            $finalResult = $this->fetchFromRedis($searchTerm);
            Redis::set($redisSearchKey, ++$searchedTimes);
        } else {
            $this->connector->fetchNewShowsAndCache($searchTerm);
            $finalResult = $this->connector->getResult();
            if ($this->connector->isSuccess()) {
                Redis::set($redisSearchKey, 1);
            }
        }
        return $finalResult;
    }

    /**
     * @param string $searchTerm
     * @return array
     */
    private function fetchFromRedis(string $searchTerm): array
    {
        $keys = Redis::keys(env('CACHED_RESULT_PREFIX') . md5($searchTerm) . ":*");
        $redisResult = [];
        foreach ($keys as $key) {
            $showDetail = Redis::get($key);
            array_push($redisResult, json_decode($showDetail));
        }
        return $redisResult;
    }
}
