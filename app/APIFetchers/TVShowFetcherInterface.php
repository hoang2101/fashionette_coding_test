<?php

namespace App\APIFetchers;

interface TVShowFetcherInterface
{
    public function fetchNewShowsAndCache(string $searchTerm): void;

    public function isSuccess(): bool;

    public function getResult(): array;
}
