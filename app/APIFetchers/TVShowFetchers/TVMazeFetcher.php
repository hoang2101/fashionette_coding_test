<?php

namespace App\APIFetchers\TVShowFetchers;

use App\APIFetchers\TVShowFetcherInterface;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;

class TVMazeFetcher implements TVShowFetcherInterface
{
    private string $endPoint = 'https://api.tvmaze.com/search/shows';
    private array $resultArray;
    private bool $isSuccess;

    public function __construct()
    {
        $this->resultArray = [];
        $this->isSuccess = false;
    }

    /**
     * Since we have already known that the API is case-sensitive tolerant and it will return the same anyway,
     * we will cache results (case-sensitive or not) in Redis if it is a successful response.
     * In the meantime, we also save the current request status and the correct results from the API response.
     * @param string $searchTerm
     */
    public function fetchNewShowsAndCache(string $searchTerm): void
    {
        $response = Http::get($this->endPoint, ['q' => $searchTerm]);
        if ($response->failed()) {
            $this->setResultAndStatus();
            return;
        }
        $rawResult = $response->json();
        $filteredResult = [];
        Redis::pipeline(function ($pipe) use ($searchTerm, $rawResult, &$filteredResult) {
            foreach ($rawResult as $singleShow) {
                $showDetail = $singleShow['show'];
                $showName = $showDetail['name'];
                $showId = $showDetail['id'];
                if (strcasecmp($showName, $searchTerm) == 0) {
                    $pipe->set(env('CACHED_RESULT_PREFIX') . md5($showName) . ":$showId", json_encode($showDetail));
                    $showName == $searchTerm && array_push($filteredResult, $showDetail);
                }
            }
        });
        $this->setResultAndStatus($filteredResult, true);
    }

    private function setResultAndStatus(array $result = [], bool $isSuccess = false)
    {
        $this->resultArray = $result;
        $this->isSuccess = $isSuccess;
    }

    public function getResult(): array
    {
        return $this->resultArray;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }
}
