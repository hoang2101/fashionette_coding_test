<?php

use Illuminate\Support\Facades\Http;

class TVMazeFetcherTest extends TestCase
{
    public function testSame(): void
    {
        $sampleTitle = 'Spider-Man';
        $expectedResult = $this->getLiveResult($sampleTitle);
        $actualResult = $this->getLocalResult($sampleTitle);
        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testEmptyTitle(): void
    {
        $response = $this->call('GET', '/', ['q' => '']);
        $this->assertEquals(422, $response->status());
    }

    public function testLongTitle(): void
    {
        $randomString = str_repeat('t', 200);
        $response = $this->call('GET', '/', ['q' => $randomString]);
        $this->assertEquals(422, $response->status());
    }


    private function getLiveResult(string $sampleTitle): array
    {
        $response = Http::get('https://api.tvmaze.com/search/shows', ['q' => $sampleTitle]);
        $showList = $response->json();
        $satisfiedList = [];
        foreach ($showList as $show) {
            if ($show['show']['name'] === $sampleTitle) {
                $satisfiedList[$show['show']['id']] = $show['show'];
            }
        }
        return $satisfiedList;
    }

    private function getLocalResult(string $sampleTitle): array
    {
        $response = $this->call('GET', '/', ['q' => $sampleTitle]);
        $showList = json_decode($response->content(), true);
        $associatedShowList = [];
        foreach ($showList as $show) {
            $associatedShowList[$show['id']] = $show;
        }
        return $associatedShowList;
    }
}
