<?php

class RouterTest extends TestCase
{
    public function testInvalidMethod(): void
    {
        $this->post('/')
            ->seeJsonEquals([
                'message' => 'Method is not allowed.',
            ])
            ->assertResponseStatus(405);
    }

    public function testInvalidURI(): void
    {
        $this->get('/something')
            ->seeJsonEquals([
                'message' => 'Endpoint does not exist.',
            ])
            ->assertResponseStatus(404);
    }
}
